using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_server_demo.Models;

namespace dotnet_server_demo.REPOSITORY
{
    public interface IRepository
    {
         IEnumerable<Student> GetAllStudents();
         Student GetStudent(Guid id);
         Student PostStudent(Student student);
         Student PutStudent(Guid id, Student student);
         void DeleteStudent(Guid id);
         IEnumerable<Student> mapDatatoRepo(IEnumerable<Data> everything);
         Student dataToDomain(Data data);
         Data domainToData(Student student);
         

    }
}