using System;
using System.ComponentModel.DataAnnotations;

namespace dotnet_server_demo.Models
{
    public class Data
    {
        public Data(string id, string name, string age)
        {
            this.Id = id;
            this.Name = name;
            this.Age = age;
        }
        // Adding constraints to the variables
        [Key]
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Age { get; private set; }    }
}