namespace dotnet_server_demo.Models
{
    public class StudentRequest
    {
        public StudentRequest(string name, int age){
            this.Name = name;
            this.Age = age;
        }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}