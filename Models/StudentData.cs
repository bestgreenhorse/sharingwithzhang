using System;
using System.Collections.Generic;
using System.Linq;

namespace dotnet_server_demo.Models
{
    public class StudentData
    {
        private Dictionary<string, Data> database 
            = new Dictionary<string, Data>{};

        public IEnumerable<Data> GetAllStudents()
        {
            return database.Values.ToList();
        }

        public Data GetStudent(string id)
        {
            return database[id];
        }

        public Data PostStudent(string id, Data student)
        {
            database.Add(id, student);
            return student;
        }

        public Data PutStudent(string id, Data student)
        {
            database[id] = student;
            return database[id];
        }

        public void DeleteStudent(string id)
        {
            database.Remove(id);
        }
    }
}